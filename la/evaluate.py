
CHESS_TYPE_NUM = 8

FIVE = 7
FOUR, THREE, TWO = 6, 4, 2
SFOUR, STHREE, STWO = 5, 3, 1

SCORE_MAX = 0x7fffffff
SCORE_MIN = -1 * SCORE_MAX
SCORE_FIVE, SCORE_FOUR, SCORE_SFOUR = 100000, 10000, 1000
SCORE_THREE, SCORE_STHREE, SCORE_TWO, SCORE_STWO = 100, 10, 8, 2
chess_len = 15

BLACK = 1
WHITE = -1
BLANK = 0


# turn表示当前最后一步我下,为1表示黑棋下，为-1表示白棋下,board为当前棋盘，为二维数组
class BoardEvaluate:
    def __init__(self, board, turn):
        # record用来标记每个点的四个方向是否被统计过棋型，count统计两种棋的各种棋型,count[0]保存我方
        self.record = [[[0, 0, 0, 0] for x in range(chess_len)] for y in range(chess_len)]
        self.count = [[0 for x in range(CHESS_TYPE_NUM)] for i in range(2)]
        self.board = board
        self.turn = turn

    def evaluate(self, checkWin=False):

        if self.turn == BLACK:
            mine = BLACK
            opponent = WHITE
        else:
            mine = WHITE
            opponent = BLACK

        for x in range(chess_len):
            for y in range(chess_len):
                if self.board[x][y] == mine:
                    self.evaluatePoint(x, y, mine, opponent)
                elif self.board[x][y] == opponent:
                    self.evaluatePoint(x, y, opponent, mine)

        if checkWin:
            return self.count[0][FIVE] > 0
        else:
            mscore, oscore = self.getScore(self.count[0], self.count[1])
            return (mscore - oscore)

    # 将统计完的数组计分，参数为我的棋型列表及对方的,返回我方分数及对方的
    def getScore(self, mine_count, opponent_count):
        mscore, oscore = 0, 0
        if mine_count[FIVE] > 0:
            return (10000, 0)
        if opponent_count[FIVE] > 0:
            return (0, 10000)
        # 两个眠四也相当于活四，即必杀
        if mine_count[SFOUR] >= 2:
            mine_count[FOUR] += 1

        if opponent_count[FOUR] > 0:
            return (0, 9050)
        if opponent_count[SFOUR] > 0:
            return (0, 9040)

        if mine_count[FOUR] > 0:
            return (9030, 0)
        if mine_count[SFOUR] > 0 and mine_count[THREE] > 0:
            return (9020, 0)

        if opponent_count[THREE] > 0 and mine_count[SFOUR] == 0:
            return (0, 9010)

        if (mine_count[THREE] > 1 and opponent_count[THREE] == 0 and opponent_count[STHREE] == 0):
            return (9000, 0)

        if mine_count[SFOUR] > 0:
            mscore += 2000

        if mine_count[THREE] > 1:
            mscore += 500
        elif mine_count[THREE] > 0:
            mscore += 100

        if opponent_count[THREE] > 1:
            oscore += 2000
        elif opponent_count[THREE] > 0:
            oscore += 400

        if mine_count[STHREE] > 0:
            mscore += mine_count[STHREE] * 10
        if opponent_count[STHREE] > 0:
            oscore += opponent_count[STHREE] * 10

        if mine_count[TWO] > 0:
            mscore += mine_count[TWO] * 4
        if opponent_count[TWO] > 0:
            oscore += opponent_count[TWO] * 4

        if mine_count[STWO] > 0:
            mscore += mine_count[STWO] * 4
        if opponent_count[STWO] > 0:
            oscore += opponent_count[STWO] * 4

        return (mscore, oscore)

    # 对点（x，y）的四个方向逐一分析
    def evaluatePoint(self, x, y, mine, opponent):
        dir_offset = [(1, 0), (0, 1), (1, 1), (1, -1)]  # 四个方向的一步距离，分别为右，上，右上，右下
        for i in range(4):
            if self.record[y][x][i] == 0:
                self.analysisLine(x, y, i, dir_offset[i], mine, opponent)

    # 根据坐标xy和方向取长度为9的线，如果线上的位置超出了棋盘范围，
    # 就将这个位置的值设为对手的值，因为超出范围和被对手棋挡着，对棋型判断的结果是一样的。
    def getLine(self, x, y, dir_offset, mine, opponent):
        line = [0 for i in range(9)]

        tmp_x = x + (-5 * dir_offset[0])
        tmp_y = y + (-5 * dir_offset[1])
        for i in range(9):
            tmp_x += dir_offset[0]
            tmp_y += dir_offset[1]
            if (tmp_x < 0 or tmp_x >= chess_len or
                    tmp_y < 0 or tmp_y >= chess_len):
                line[i] = opponent
            else:
                line[i] = self.board[tmp_y][tmp_x]

        return line

    # 对点(x,y)及给定方向取线并分析棋型
    def analysisLine(self, x, y, dir_index, dir_offset, mine, opponent):
        # setRecord标记已经检测过需要跳过的棋子某方向
        def setRecord(self, x, y, left, right, dir_index, dir_offset):
            tmp_x = x + (-5 + left) * dir_offset[0]
            tmp_y = y + (-5 + left) * dir_offset[1]
            for i in range(left, right + 1):
                tmp_x += dir_offset[0]
                tmp_y += dir_offset[1]
                self.record[tmp_y][tmp_x][dir_index] = 1

        empty = 0
        left_idx, right_idx = 4, 4

        line = self.getLine(x, y, dir_offset, mine, opponent)

        # 统计mine棋子左右界限
        while right_idx < 8:
            if line[right_idx + 1] != mine:
                break
            right_idx += 1
        while left_idx > 0:
            if line[left_idx - 1] != mine:
                break
            left_idx -= 1

        # 统计从中心开始mine棋子范围外空格左右界限
        left_range, right_range = left_idx, right_idx
        while right_range < 8:
            if line[right_range + 1] == opponent:
                break
            right_range += 1
        while left_range > 0:
            if line[left_range - 1] == opponent:
                break
            left_range -= 1

        chess_range = right_range - left_range + 1
        # 长度过短无法匹配棋型，标记后退出
        if chess_range < 5:
            setRecord(self, x, y, left_range, right_range, dir_index, dir_offset)
            return 0

        setRecord(self, x, y, left_idx, right_idx, dir_index, dir_offset)

        m_range = right_idx - left_idx + 1

        # M:mine chess, P:opponent chess or out of borad, X: empty
        if m_range == 5:
            self.count[0][FIVE] += 1

        # Live Four : XMMMMX
        # Chong Four : XMMMMP, PMMMMX
        if m_range == 4:
            left_empty = right_empty = False
            if line[left_idx - 1] == empty:
                left_empty = True
            if line[right_idx + 1] == empty:
                right_empty = True
            if left_empty and right_empty:
                self.count[0][FOUR] += 1
            elif left_empty or right_empty:
                self.count[0][SFOUR] += 1

        # Chong Four : MXMMM, MMMXM, the two types can both exist
        # Live Three : XMMMXX, XXMMMX
        # Sleep Three : PMMMX, XMMMP, PXMMMXP
        if m_range == 3:
            left_empty = right_empty = False
            left_four = right_four = False
            if line[left_idx - 1] == empty:
                if line[left_idx - 2] == mine:  # MXMMM
                    setRecord(self, x, y, left_idx - 2, left_idx - 1, dir_index, dir_offset)
                    self.count[0][SFOUR] += 1
                    left_four = True
                left_empty = True

            if line[right_idx + 1] == empty:
                if line[right_idx + 2] == mine:  # MMMXM
                    setRecord(self, x, y, right_idx + 1, right_idx + 2, dir_index, dir_offset)
                    self.count[0][SFOUR] += 1
                    right_four = True
                right_empty = True

            if left_four or right_four:
                pass
            elif left_empty and right_empty:
                if chess_range > 5:  # XMMMXX, XXMMMX
                    self.count[0][THREE] += 1
                else:  # PXMMMXP
                    self.count[0][STHREE] += 1
            elif left_empty or right_empty:  # PMMMX, XMMMP
                self.count[0][STHREE] += 1

        # Chong Four: MMXMM, only check right direction
        # Live Three: XMXMMX, XMMXMX the two types can both exist
        # Sleep Three: PMXMMX, XMXMMP, PMMXMX, XMMXMP
        # Live Two: XMMX
        # Sleep Two: PMMX, XMMP
        if m_range == 2:
            left_empty = right_empty = False
            left_three = right_three = False
            if line[left_idx - 1] == empty:
                if line[left_idx - 2] == mine:
                    setRecord(self, x, y, left_idx - 2, left_idx - 1, dir_index, dir_offset)
                    if line[left_idx - 3] == empty:
                        if line[right_idx + 1] == empty:  # XMXMMX
                            self.count[0][THREE] += 1
                        else:  # XMXMMP
                            self.count[0][STHREE] += 1
                        left_three = True
                    elif line[left_idx - 3] == opponent:  # PMXMMX
                        if line[right_idx + 1] == empty:
                            self.count[0][STHREE] += 1
                            left_three = True

                left_empty = True

            if line[right_idx + 1] == empty:
                if line[right_idx + 2] == mine:
                    if line[right_idx + 3] == mine:  # MMXMM
                        setRecord(self, x, y, right_idx + 1, right_idx + 2, dir_index, dir_offset)
                        self.count[0][SFOUR] += 1
                        right_three = True
                    elif line[right_idx + 3] == empty:
                        # setRecord(self, x, y, right_idx+1, right_idx+2, dir_index, dir)
                        if left_empty:  # XMMXMX
                            self.count[0][THREE] += 1
                        else:  # PMMXMX
                            self.count[0][STHREE] += 1
                        right_three = True
                    elif left_empty:  # XMMXMP
                        self.count[0][STHREE] += 1
                        right_three = True

                right_empty = True

            if left_three or right_three:
                pass
            elif left_empty and right_empty:  # XMMX
                self.count[0][TWO] += 1
            elif left_empty or right_empty:  # PMMX, XMMP
                self.count[0][STWO] += 1

        # Live Two: XMXMX, XMXXMX only check right direction
        # Sleep Two: PMXMX, XMXMP
        if m_range == 1:
            left_empty = right_empty = False
            if line[left_idx - 1] == empty:
                if line[left_idx - 2] == mine:
                    if line[left_idx - 3] == empty:
                        if line[right_idx + 1] == opponent:  # XMXMP
                            self.count[0][STWO] += 1
                left_empty = True

            if line[right_idx + 1] == empty:
                if line[right_idx + 2] == mine:
                    if line[right_idx + 3] == empty:
                        if left_empty:  # XMXMX
                            # setRecord(self, x, y, left_idx, right_idx+2, dir_index, dir)
                            self.count[0][TWO] += 1
                        else:  # PMXMX
                            self.count[0][STWO] += 1
                elif line[right_idx + 2] == empty:
                    if line[right_idx + 3] == mine and line[right_idx + 4] == empty:  # XMXXMX
                        self.count[0][TWO] += 1

        return 0


